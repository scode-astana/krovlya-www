<?php

namespace admin\Controllers\Information;

use core\Controller;

class Information extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Information\Information();
        parent::__construct();
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->about_us = trim($_POST['about_us']);
            $this->model->address = trim($_POST['address']);
            $this->model->phone = trim($_POST['phone']);
            $this->model->email = trim($_POST['email']);
            if ($_FILES['image_about_us']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/information/image_about_us/";
                $img_file = $img_dir . basename($_FILES["image_about_us"]["name"]);

                if (move_uploaded_file($_FILES['image_about_us']['tmp_name'], $img_file)) {
                    $this->model->image_about_us = "/public/img/uploads/information/image_about_us/" . basename($_FILES["image_about_us"]["name"]);
                }
            } else {
                $this->model->image_about_us = \admin\Models\Information\Information::findBy('id', $id)[0]['image_about_us'];
            }

            if ($_FILES['image_header']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/information/image_header/";
                $img_file = $img_dir . basename($_FILES["image_header"]["name"]);

                if (move_uploaded_file($_FILES['image_header']['tmp_name'], $img_file)) {
                    $this->model->image_header = "/public/img/uploads/information/image_header/" . basename($_FILES["image_header"]["name"]);
                }
            } else {
                $this->model->image_header = \admin\Models\Information\Information::findBy('id', $id)[0]['image_header'];
            }

            if ($_FILES['image_footer']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/information/image_footer/";
                $img_file = $img_dir . basename($_FILES["image_footer"]["name"]);

                if (move_uploaded_file($_FILES['image_footer']['tmp_name'], $img_file)) {
                    $this->model->image_footer = "/public/img/uploads/information/image_footer/" . basename($_FILES["image_footer"]["name"]);
                }
            } else {
                $this->model->image_footer = \admin\Models\Information\Information::findBy('id', $id)[0]['image_footer'];
            }

            $this->model->update($id);
            header('Location: /admin.php/information/?update=true');
        } else {
            $this->view->information = \admin\Models\Information\Information::findBy('id', '1');
            $this->view->display(ROOT . '/admin/View/information/edit.php');
        }
    }

}