<?php

namespace admin\Controllers\Slider2;

use core\Controller;

class Slider2 extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Slider2\Slider2();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->slider2 = \admin\Models\Slider2\Slider2::findAll();
        $this->view->display(ROOT . '/admin/View/slider2/index.php');
    }

    public function Insert()
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $this->model->insert();
                $last_id = \admin\Models\Slider2\Slider2::getLast()[0]['id'];

                $img_dir = ROOT . "/public/img/uploads/slider2/$last_id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/slider2/$last_id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/slider2/$last_id/" . basename($_FILES["image"]["name"]);
                }

                $this->model->update($last_id);
            } else {
                $this->model->insert();
            }

            header('Location: /admin.php/slider2/?insert=true');
        } else {
            $this->view->display(ROOT . '/admin/View/slider2/add.php');
        }
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/slider2/$id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/slider2/$id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/slider2/$id/" . basename($_FILES["image"]["name"]);
                }
            } else {
                $this->model->image = \admin\Models\Slider2\Slider2::findBy('id', $id)[0]['image'];
            }

            $this->model->update($id);
            header('Location: /admin.php/slider2/' . $id . '/?update=true');
        } else {
            $this->view->slider2 = \admin\Models\Slider2\Slider2::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/slider2/edit.php');
        }
    }

    public function Delete($id)
    {
        $this->model->deleteBy('id', $id);
        header('Location: /admin.php/slider2/?delete=true');
    }
}