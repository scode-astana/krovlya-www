<?php

namespace admin\Controllers\Differencies;

use core\Controller;

class Differencies extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Differencies\Differencies();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->differencies = \admin\Models\Differencies\Differencies::findAll();
        $this->view->display(ROOT . '/admin/View/differencies/index.php');
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim($_POST['title']);
            $this->model->update($id);
            header('Location: /admin.php/differencies/' . $id . '/?update=true');
        } else {
            $this->view->differencies = \admin\Models\Differencies\Differencies::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/differencies/edit.php');
        }
    }
}