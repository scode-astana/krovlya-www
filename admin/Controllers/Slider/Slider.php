<?php

namespace admin\Controllers\Slider;

use core\Controller;

class Slider extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Slider\Slider();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->slides = \admin\Models\Slider\Slider::findAll();
        $this->view->display(ROOT . '/admin/View/slider/index.php');
    }

    public function Insert()
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $this->model->insert();
                $last_id = \admin\Models\Slider\Slider::getLast()[0]['id'];

                $img_dir = ROOT . "/public/img/uploads/slider/$last_id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/slider/$last_id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/slider/$last_id/" . basename($_FILES["image"]["name"]);
                }

                $this->model->update($last_id);
            } else {
                $this->model->insert();
            }

            header('Location: /admin.php/slider/?insert=true');
        } else {
            $this->view->display(ROOT . '/admin/View/slider/add.php');
        }
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/slider/$id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/slider/$id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/slider/$id/" . basename($_FILES["image"]["name"]);
                }
            } else {
                $this->model->image = \admin\Models\Slider\Slider::findBy('id', $id)[0]['image'];
            }

            $this->model->update($id);
            header('Location: /admin.php/slide/' . $id . '/?update=true');
        } else {
            $this->view->slide = \admin\Models\Slider\Slider::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/slider/edit.php');
        }
    }

    public function Delete($id)
    {
        $this->model->deleteBy('id', $id);
        header('Location: /admin.php/slider/?delete=true');
    }
}