<?php

namespace admin\Controllers\Users;

use core\Controller;


class Users extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Users\Users();
        parent::__construct();
    }
}