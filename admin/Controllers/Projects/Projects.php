<?php

namespace admin\Controllers\Projects;

use core\Controller;

class Projects extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Projects\Projects();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->projects = \admin\Models\Projects\Projects::findAll();
        $this->view->display(ROOT . '/admin/View/projects/index.php');
    }

    public function Insert()
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $this->model->insert();
                $last_id = \admin\Models\Projects\Projects::getLast()[0]['id'];

                $img_dir = ROOT . "/public/img/uploads/projects/$last_id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/projects/$last_id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/projects/$last_id/" . basename($_FILES["image"]["name"]);
                }

                $this->model->update($last_id);
            } else {
                $this->model->insert();
            }

            header('Location: /admin.php/projects/?insert=true');
        } else {
            $this->view->display(ROOT . '/admin/View/projects/add.php');
        }
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);

            if ($_FILES['image']['name'] != '') {
                $img_dir = ROOT . "/public/img/uploads/projects/$id/";
                if (!file_exists($img_dir)) {
                    mkdir(ROOT . "/public/img/uploads/projects/$id/");
                }
                $img_file = $img_dir . basename($_FILES["image"]["name"]);

                if (move_uploaded_file($_FILES['image']['tmp_name'], $img_file)) {
                    $this->model->image = "/public/img/uploads/projects/$id/" . basename($_FILES["image"]["name"]);
                }
            } else {
                $this->model->image = \admin\Models\Projects\Projects::findBy('id', $id)[0]['image'];
            }

            $this->model->update($id);
            header('Location: /admin.php/projects/' . $id . '/?update=true');
        } else {
            $this->view->projects = \admin\Models\Projects\Projects::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/projects/edit.php');
        }
    }

    public function Delete($id)
    {
        $this->model->deleteBy('id', $id);
        header('Location: /admin.php/projects/?delete=true');
    }
}