<?php

namespace admin\Controllers\Modules;

use core\Controller;

class Modules extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Modules\Modules();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->modules_new = \admin\Models\Modules\Modules::findBy('modules_group', '1');
        $this->view->modules_slider = \admin\Models\Modules\Modules::findBy('modules_group', '2');
        $this->view->modules_price = \admin\Models\Modules\Modules::findBy('modules_group', '3');
        $this->view->display(ROOT . '/admin/View/modules/index.php');
    }

    public function Insert()
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);
            $this->model->position = trim($_POST['position']);
            $this->model->insert();

            header('Location: /admin.php/modules/?insert=true');
        } else {
            $this->view->display(ROOT . '/admin/View/modules/add.php');
        }
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->title = trim(strip_tags($_POST['title']));
            $this->model->text = trim($_POST['text']);
            $this->model->position = trim($_POST['position']);
            $this->model->update($id);
            header('Location: /admin.php/modules/' . $id . '/?update=true');
        } else {
            $this->view->module = \admin\Models\Modules\Modules::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/modules/edit.php');
        }
    }

    public function Delete($id)
    {
        $this->model->deleteBy('id', $id);
        header('Location: /admin.php/modules/?delete=true');
    }
}