<?php

namespace admin\Controllers;

use admin\Models\Modules\Modules;
use admin\Models\Slider2\Slider2;
use admin\Models\Projects\Projects;
use admin\Models\Slider\Slider;
use admin\Models\Users\Users;
use core\Controller;


class Index extends Controller
{
    public function __construct()
    {
        $this->model = new \admin\Models\Index();
        parent::__construct();
    }

    public function Index()
    {
        $user_per = \admin\Models\Users\Users::findBy('id', $_SESSION['id'])[0]['user_group'];

        if (!empty($_POST)) {
            $user = new Users();
            $user->login = trim(strip_tags($_POST['login']));
            $user->password = trim(strip_tags($_POST['password']));

            if (true == $user->check()) {

                $admin = \admin\Models\Users\Users::findBy('login', $user->login);
                $_SESSION['id'] = $admin[0]['id'];

                if ($admin[0]['user_group'] == 100) {
                    $this->view->modules = array_slice(Modules::findAll(), 0, 3);
                    $this->view->slider = array_slice(Slider::findAll(), 0, 3);
                    $this->view->title = 'Главная страница';
                    $this->view->display(__DIR__ . '/../View/index.php');
                } else {
                    echo 'Доступ закрыт';
                    $this->view->display(__DIR__ . '/../View/login.php');
                }

            } else {
                echo 'Неверный логин';
                $this->view->display(__DIR__ . '/../View/login.php');
            }

        } else if (100 == $user_per) {
            $this->view->modules = array_slice(Modules::findAll(), 0, 3);
            $this->view->slider = array_slice(Slider::findAll(), 0, 3);
            $this->view->projects = array_slice(Projects::findAll(), 0, 3);
            $this->view->slider2 = array_slice(Slider2::findAll(), 0, 3);
            $this->view->title = 'Главная страница';
            $this->view->display(__DIR__ . '/../View/index.php');
        } else {
            $this->view->display(__DIR__ . '/../View/login.php');
        }
    }
}