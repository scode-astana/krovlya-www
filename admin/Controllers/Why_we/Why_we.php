<?php

namespace admin\Controllers\Why_we;

use core\Controller;

class Why_we extends Controller
{

    public function __construct()
    {
        $this->model = new \admin\Models\Why_we\Why_we();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->why_we = \admin\Models\Why_we\Why_we::findAll();
        $this->view->display(ROOT . '/admin/View/why_we/index.php');
    }

    public function Update($id)
    {
        if ($_POST) {
            $this->model->text = trim($_POST['text']);
            $this->model->update($id);
            header('Location: /admin.php/why_we/' . $id . '/?update=true');
        } else {
            $this->view->why_we = \admin\Models\Why_we\Why_we::findBy('id', $id);
            $this->view->display(ROOT . '/admin/View/why_we/edit.php');
        }
    }
}