<?php
namespace admin\Models\Slider2;
use core\Model;

class Slider2 extends Model
{

    const TABLE = 'slider2';

    public $title;
    public $image;
}