<?php
namespace admin\Models\Information;
use core\Model;

class Information extends Model
{

    const TABLE = 'information';

    public $title;
    public $about_us;
    public $address;
    public $phone;
    public $email;
    public $image_about_us;
    public $image_header;
    public $image_footer;
}