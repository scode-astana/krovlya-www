<?php
namespace admin\Models\Projects;
use core\Model;

class Projects extends Model
{

    const TABLE = 'projects';

    public $title;
    public $text;
    public $image;
}