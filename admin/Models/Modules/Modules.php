<?php
namespace admin\Models\Modules;
use core\Model;

class Modules extends Model
{

    const TABLE = 'modules';

    public $title;
    public $text;
    public $position;
}