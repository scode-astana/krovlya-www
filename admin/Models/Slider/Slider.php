<?php
namespace admin\Models\Slider;
use core\Model;

class Slider extends Model
{

    const TABLE = 'slider';

    public $title;
    public $text;
    public $image;
}