<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['insert'] == 'true'): ?>
    <div class="alert alert-info">Модуль добавлен</div>
<?php elseif ($_GET['delete'] == 'true'): ?>
    <div class="alert alert-info">Модуль удален</div>
<?php endif; ?>

<div class="panel panel-default">
    <div class="panel-heading">Черепица ПВХ</div>
    <div class="panel-body">
        <?php if ($modules_new): ?>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Позиция</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($modules_new as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-6"><a href="/admin.php/modules/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-2"><?php echo $item['position']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/modules/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Слайдер</div>
    <div class="panel-body">
        <?php if ($modules_slider): ?>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Позиция</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($modules_slider as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-6"><a href="/admin.php/modules/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-2"><?php echo $item['position']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/modules/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Прайс</div>
    <div class="panel-body">
        <?php if ($modules_price): ?>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Позиция</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($modules_price as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-6"><a href="/admin.php/modules/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-2"><?php echo $item['position']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/modules/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>
