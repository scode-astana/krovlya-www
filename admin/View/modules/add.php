<?php include_once ROOT . '/admin/View/header.php'; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/modules/insert" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title" class="col-sm-2">Заголовок:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" required>
                    </div>
                </div>

                <div class="form-group padding15">
                    <textarea name="text" cols="30" rows="10"></textarea>
                </div>

                <div class="form-group">
                    <label for="position" class="col-sm-2">Позиция:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="position" id="position">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Добавить модуль</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>