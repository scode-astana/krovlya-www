<?php include_once ROOT . '/admin/View/header.php'; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/slide/insert" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title" class="col-sm-2">Заголовок:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" required>
                    </div>
                </div>

                <div class="form-group padding15">
                    <textarea name="text" cols="30" rows="10"></textarea>
                </div>

                <div class="form-group">
                    <label for="image" class="col-sm-2">Изображения:</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="image" id="image" />
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Добавить слайд</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>