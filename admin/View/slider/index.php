<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['insert'] == 'true'): ?>
    <div class="alert alert-info">Слайд добавлен</div>
<?php elseif ($_GET['delete'] == 'true'): ?>
    <div class="alert alert-info">Слайд удален</div>
<?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-heading">Слайды</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Текст</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($slides as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-3"><a href="/admin.php/slide/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-5"><?php echo $item['text']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/slide/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                            <a href="/admin.php/slide/delete/<?php echo $item['id']; ?>" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <a class="btn btn-success" href="/admin.php/slide/insert">Добавить слайд</a>

<?php include_once ROOT . '/admin/View/footer.php'; ?>
