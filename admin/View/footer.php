</div>
</div>
<footer>
    <div class="col-md-12 col-sm-12 footer-img">
    </div>
</footer>

<script>
    tinymce.init({
        selector: "textarea",theme: "modern",height: 300,
        editor_deselector : "noneEditor",
        entity_encoding: "named",
        language : "ru",
        remove_linebreaks: false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        image_advtab: true ,
        relative_urls: false,
        external_filemanager_path:"/public/filemanager/",
        filemanager_title:"Менеджер файлов" ,
        external_plugins: { "filemanager" : "/public/filemanager/plugin.min.js"}
    });
</script>