<?php include_once 'header.php'; ?>

<?php if ($slider): ?>
    <div class="panel panel-default">
        <div class="panel-heading">Слайды</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Текст</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($slider as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-3"><a href="/admin.php/slide/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-5"><?php echo $item['text']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/slide/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                            <a href="/admin.php/slide/delete/<?php echo $item['id']; ?>" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <a href="/admin.php/slider" class="btn btn-default btn-block">Все слайды</a>
        </div>
    </div>
<?php endif; ?>

<?php if ($projects): ?>
    <div class="panel panel-default">
        <div class="panel-heading">Проекты</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Текст</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($projects as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-3"><a href="/admin.php/projects/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-5"><?php echo $item['text']; ?></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/projects/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                            <a href="/admin.php/projects/delete/<?php echo $item['id']; ?>" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <a href="/admin.php/projects" class="btn btn-default btn-block">Все проекты</a>
        </div>
    </div>
<?php endif; ?>

<?php if ($partners): ?>
    <div class="panel panel-default">
        <div class="panel-heading">Партнеры</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($partners as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-8"><a href="/admin.php/partners/<?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/partners/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                            <a href="/admin.php/partners/delete/<?php echo $item['id']; ?>" class="btn btn-danger">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <a href="/admin.php/partners" class="btn btn-default btn-block">Все партнеры</a>
        </div>
    </div>
<?php endif; ?>

<?php if ($modules): ?>
    <div class="panel panel-default">
        <div class="panel-heading">Модули</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Заголовок</th>
                    <th>Позиция</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($modules as $module): ?>
                        <tr>
                            <td class="col-sm-1"><?php echo $module['id']; ?></td>
                            <td class="col-sm-6"><a href="/admin.php/modules/<?php echo $module['id']; ?>"><?php echo $module['title']; ?></a></td>
                            <td class="col-sm-2"><?php echo $module['position']; ?></td>
                            <td class="col-sm-3">
                                <a href="/admin.php/modules/<?php echo $module['id']; ?>" class="btn btn-info">Редактировать</a>
                                <a href="/admin.php/modules/delete/<?php echo $module['id']; ?>" class="btn btn-danger">Удалить</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <a href="/admin.php/modules" class="btn btn-default btn-block">Все модули</a>
        </div>
    </div>
<?php endif; ?>

<?php include_once 'footer.php'; ?>