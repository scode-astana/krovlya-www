<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['update'] == 'true') : ?>
    <div class="alert alert-info">Слайд обновлен</div>
<?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/slider2/<?php echo $slider2[0]['id']; ?>" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title" class="col-sm-2">Заголовок:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" value='<?php echo $slider2[0]['title']; ?>' required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="image" class="col-sm-2">Изображения:</label>
                    <div class="col-sm-10">
                        <?php if ($slider2[0]['image'] != '') : ?>
                            <img class="img-thumbnail" src="<?php echo $slider2[0]['image']; ?>" />
                        <?php endif; ?>
                        <input type="file" class="form-control" name="image" id="image" />
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>