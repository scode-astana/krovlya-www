<?php
    $menu = [
        [
            'title' => 'Главная',
            'url'   => ''
        ],
        [
            'title' => 'Информация',
            'url'   => 'information'
        ],
        [
            'title' => 'Слайды',
            'url'   => 'slider'
        ],
        [
            'title' => 'Материалы',
            'url'   => 'slider2'
        ],
        [
            'title' => 'Проекты',
            'url'   => 'projects'
        ],
        [
            'title' => 'Почему мы?',
            'url'   => 'why_we'
        ],
        [
            'title' => 'Чем мы отличаемся',
            'url'   => 'differencies'
        ],
        [
            'title' => 'Модули',
            'url'   => 'modules'
        ]
    ];
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=0">
    <link rel="stylesheet" type="text/css" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/public/css/admin.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/public/js/tinymce/tinymce.min.js"></script>
    <title>Админ панель</title>
</head>

<body>

<div id="content" class="<?php echo explode('/', $_SERVER['REQUEST_URI'])[2] ? 'page' : ''; ?>">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/admin.php">Кровля</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <?php foreach ($menu as $item) : ?>
                            <?php if ($item['url'] == explode('/', $_SERVER['REQUEST_URI'])[2]) : ?>
                                <li class="active"><a href="/admin.php/<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
                            <?php else: ?>
                                <li><a href="/admin.php/<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="container">