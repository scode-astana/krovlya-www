<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['update'] == 'true') : ?>
    <div class="alert alert-info">Информация обновлена</div>
<?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/differencies/<?php echo $differencies[0]['id']; ?>" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="address" class="col-sm-2">Текст:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" value="<?php echo $differencies[0]['title']; ?>">
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>