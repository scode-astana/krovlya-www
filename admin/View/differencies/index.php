<?php include_once ROOT . '/admin/View/header.php'; ?>


    <div class="panel panel-default">
        <div class="panel-heading">Чем мы отличаемся?</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Текст</th>
                    <th>Картинка</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($differencies as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-5"><?php echo $item['title']; ?></td>
                        <td class="col-sm-3"><img style="background: #ddd;" class="img-thumbnail" src="<?php echo $item['image']; ?>" /></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/differencies/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>
