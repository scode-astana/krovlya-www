<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['update'] == 'true') : ?>
    <div class="alert alert-info">Информация обновлена</div>
<?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/information" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title" class="col-sm-2">Название сайта:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" value="<?php echo $information[0]['title']; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="about_us" class="col-sm-2">Коротко о нас:</label>
                    <div class="col-sm-10">
                        <div class="form-group padding15">
                            <textarea name="about_us" cols="30" rows="10"><?php echo $information[0]['about_us']; ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="image_about_us" class="col-sm-2">Изображения о нас:</label>
                    <div class="col-sm-10">
                        <?php if ($information[0]['image_about_us'] != '') : ?>
                            <img class="img-thumbnail" src="<?php echo $information[0]['image_about_us']; ?>" />
                        <?php endif; ?>
                        <input type="file" class="form-control" name="image_about_us" id="image_about_us" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="image_header" class="col-sm-2">Лого в шапку:</label>
                    <div class="col-sm-10">
                        <?php if ($information[0]['image_header'] != '') : ?>
                            <img class="img-thumbnail" src="<?php echo $information[0]['image_header']; ?>" />
                        <?php endif; ?>
                        <input type="file" class="form-control" name="image_header" id="image_header" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="image_footer" class="col-sm-2">Лого в футер:</label>
                    <div class="col-sm-10">
                        <?php if ($information[0]['image_footer'] != '') : ?>
                            <img class="img-thumbnail" src="<?php echo $information[0]['image_footer']; ?>" />
                        <?php endif; ?>
                        <input type="file" class="form-control" name="image_footer" id="image_footer" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-sm-2">Адрес:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" id="address" value="<?php echo $information[0]['address']; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-sm-2">Телефон:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $information[0]['phone']; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2">E-mail:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" id="email" value="<?php echo $information[0]['email']; ?>" required>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>