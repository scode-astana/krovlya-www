<?php include_once ROOT . '/admin/View/header.php'; ?>


    <div class="panel panel-default">
        <div class="panel-heading">Почему мы?</div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Текст</th>
                    <th>Картинка</th>
                    <th>Операция</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($why_we as $item): ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $item['id']; ?></td>
                        <td class="col-sm-5"><?php echo $item['text']; ?></td>
                        <td class="col-sm-3"><img class="img-thumbnail" src="<?php echo $item['image']; ?>" /></td>
                        <td class="col-sm-3">
                            <a href="/admin.php/why_we/<?php echo $item['id']; ?>" class="btn btn-info">Редактировать</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>
