<?php include_once ROOT . '/admin/View/header.php'; ?>

<?php if ($_GET['update'] == 'true') : ?>
    <div class="alert alert-info">Информация обновлена</div>
<?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <form action="/admin.php/why_we/<?php echo $why_we[0]['id']; ?>" method="post" class="admin-form form-horizontal" enctype="multipart/form-data">

                <div class="form-group padding15">
                    <textarea name="text" cols="30" rows="10"><?php echo $why_we[0]['text']; ?></textarea>
                </div>

                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>

<?php include_once ROOT . '/admin/View/footer.php'; ?>