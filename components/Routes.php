<?php
return array(
    /* ========== ADMIN ========== */

    /* Slider */
    '/admin.php/slider' => '\admin\Controllers\Slider\Slider',
    '/admin.php/slide/:num' => '\admin\Controllers\Slider\Slider/Update/$1',
    '/admin.php/slide/insert' => '\admin\Controllers\Slider\Slider/Insert',
    '/admin.php/slide/delete/:num' => '\admin\Controllers\Slider\Slider/Delete/$1',

    /* Projects */
    '/admin.php/projects' => '\admin\Controllers\Projects\Projects',
    '/admin.php/projects/:num' => '\admin\Controllers\Projects\Projects/Update/$1',
    '/admin.php/projects/insert' => '\admin\Controllers\Projects\Projects/Insert',
    '/admin.php/projects/delete/:num' => '\admin\Controllers\Projects\Projects/Delete/$1',

    /* Why we */
    '/admin.php/why_we' => '\admin\Controllers\Why_we\Why_we',
    '/admin.php/why_we/:num' => '\admin\Controllers\Why_we\Why_we/Update/$1',

    /* Slider2 */
    '/admin.php/slider2' => '\admin\Controllers\Slider2\Slider2',
    '/admin.php/slider2/:num' => '\admin\Controllers\Slider2\Slider2/Update/$1',
    '/admin.php/slider2/insert' => '\admin\Controllers\Slider2\Slider2/Insert',
    '/admin.php/slider2/delete/:num' => '\admin\Controllers\Slider2\Slider2/Delete/$1',

    /* Differencies */
    '/admin.php/differencies' => '\admin\Controllers\Differencies\Differencies',
    '/admin.php/differencies/:num' => '\admin\Controllers\Differencies\Differencies/Update/$1',
    '/admin.php/differencies/insert' => '\admin\Controllers\Differencies\Differencies/Insert',
    '/admin.php/differencies/delete/:num' => '\admin\Controllers\Differencies\Differencies/Delete/$1',

    /* Information */
    '/admin.php/information' => '\admin\Controllers\Information\Information/Update/1',

    /* Modules */
    '/admin.php/modules' => '\admin\Controllers\Modules\Modules',
    '/admin.php/modules/:num' => '\admin\Controllers\Modules\Modules/Update/$1',
    '/admin.php/modules/insert' => '\admin\Controllers\Modules\Modules/Insert',
    '/admin.php/modules/delete/:num' => '\admin\Controllers\Modules\Modules/Delete/$1',

    /* Ads */
    '/admin.php/ads' => '\admin\Controllers\Ads\Ads',
    '/admin.php/ads/:num' => '\admin\Controllers\Ads\Ads/Update/$1',
    '/admin.php/ads/insert' => '\admin\Controllers\Ads\Ads/Insert',
    '/admin.php/ads/delete/:num' => '\admin\Controllers\Ads\Ads/Delete/$1',

    /* Comments */
    '/admin.php/comments' => '\admin\Controllers\Comments\Comments',
    '/admin.php/comments/:num' => '\admin\Controllers\Comments\Comments/Update/$1',
    '/admin.php/comments/insert' => '\admin\Controllers\Comments\Comments/Insert',
    '/admin.php/comments/delete/:num' => '\admin\Controllers\Comments\Comments/Delete/$1',


    /* ========== SITE ========== */

    '' => '\site\Controllers\Index',
    '/index.php/ads/:num' => '\site\Controllers\Ads\Ads/View/$1',
    '/index.php/comments/insert' => '\site\Controllers\Comments\Comments/Insert',
);
?>