<?php
ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require_once 'components/Autoload.php';
require_once 'components/Router.php';
define('ROOT', dirname(__FILE__));
$url = explode('/', $_SERVER['REQUEST_URI']);
date_default_timezone_set('Asia/Almaty');

$routes = include(ROOT . '/components/Routes.php');

Router::addRoute($routes);
Router::dispatch();