<?php

namespace site\Controllers;

use admin\Models\Differencies\Differencies;
use admin\Models\Modules\Modules;
use admin\Models\Slider2\Slider2;
use admin\Models\Why_we\Why_we;
use admin\Models\Projects\Projects;
use admin\Models\Slider\Slider;
use core\Controller;


class Index extends Controller
{
    public function __construct()
    {
        $this->model = new \admin\Models\Index();
        parent::__construct();
    }

    public function Index()
    {
        $this->view->slider = Slider::findAll('id', 'DESC');
        $this->view->projects = Projects::findAll('id', 'DESC');
        $this->view->slider2 = Slider2::findAll();
        $this->view->why_we = Why_we::findAll();
        $this->view->differencies = Differencies::findAll();
        $this->view->new_block = Modules::findInTable('modules_group', 'id', '1');
        $this->view->slider_module = Modules::findBy('modules_group', '2');
        $this->view->new = Modules::findBy('modules_group', '1');
        $this->view->price_list = Modules::findBy('modules_group', '3');
        $this->view->display(ROOT . '/site/View/index.php');
    }
}