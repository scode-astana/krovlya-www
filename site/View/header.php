<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=0">
    <link rel="stylesheet" type="text/css" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/public/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/public/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <link rel="stylesheet" type="text/css" href="/public/css/media.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/owl.carousel.js"></script>
    <script type="text/javascript" src="/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/public/js/script.js"></script>

    <title>Мир кровли</title>
</head>

<body>

<header>
    <div class="container-fluid">
        <div class="col-sm-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mk-nav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="logo" href="/"><img src="<?php echo $information[0]['image_header']; ?>" alt="Мир кровли"></a>
                    </div>

                    <div class="collapse navbar-collapse" id="mk-nav">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/">Главная</a></li>
                            <li><a href="#block_about">О нас</a></li>
                            <li><a href="#block_projects">Проекты</a></li>
                            <li><a href="#block_footer">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>