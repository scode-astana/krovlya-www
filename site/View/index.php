<?php include_once ROOT . '/site/View/header.php'; ?>

<section class="slider">
    <div id="capital-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php for($i = 0; $i < count($slider); $i++): ?>
                <?php if($i == 0): ?>
                    <li data-target="#capital-slider" data-slide-to="<?php echo $i; ?>" class="active"></li>
                <?php else: ?>
                    <li data-target="#capital-slider" data-slide-to="<?php echo $i; ?>"></li>
                <?php endif; ?>
            <?php endfor; ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-heading">
                <h1><?php echo $slider_module[0]['title']; ?></h1>
                <div class="utp--carousel"><?php echo $slider_module[0]['text']; ?></div>
                <button onclick="$('.cbk-phone-bgr').trigger('click');"><?php echo $slider_module[1]['title']; ?></button>
            </div>
            <?php for($i = 0; $i < count($slider); $i++): ?>
                <?php if($i == 0): ?>
                    <div class="item active">
                        <img src="<?php echo $slider[$i]['image']; ?>">
                        <div class="caption--carousel">
                            <?php echo $slider[$i]['text']; ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="item">
                        <img src="<?php echo $slider[$i]['image']; ?>">
                        <div class="caption--carousel">
                            <?php echo $slider[$i]['text']; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
        <a class="left carousel-control" href="#capital-slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#capital-slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</section>

<section class="about" id="block_about">
    <div class="col-md-6 col-sm-12 text--about">
        <h3>О компании</h3>
        <div class="p--about"><?php echo $information[0]['about_us']; ?>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 img--about">
        <div class="img-cont--about">
            <img src="<?php echo $information[0]['image_about_us']; ?>">
        </div>
    </div>
</section>

<section class="whywe" id="block_whywe">
    <h3>Почему мы?</h3>

    <?php for ($i = 0; $i < count($why_we); $i++): ?>
        <?php echo $i == 0 ? '<div class="col-md-12">' : ''; ?>
        <?php if ($i % 3 == 0): ?>
            </div>
            <div class="col-md-12">
        <?php endif; ?>
        <div class="col-md-4 col-sm-6 col-xs-12 cont--whywe">
            <div class="img--whywe">
                <img src="<?php echo $why_we[$i]['image']; ?>">
            </div>
            <div class="p--whywe"><?php echo $why_we[$i]['text']; ?></div>
        </div>
        <?php echo $i == 9 ? '</div>' : ''; ?>
    <?php endfor; ?>
</section>

<section class="projects" id="block_projects">
    <div class="cont--projects">
        <?php foreach ($projects as $item): ?>
            <div class="col-md-3 col-sm-6 col-xs-12 project--projects">
                <img src="<?php echo $item['image']; ?>">
                <div class="hover--projects">
                    <h5><?php echo $item['title']; ?></h5>
                    <div class="p--projects"><?php echo $item['text']; ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="row materials">
    <div class="col-md-2 col-sm-2 col-xs-2 button--carousel">
        <i id="left--materials" class="glyphicon glyphicon-triangle-left"></i>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-8 cont--materials">
        <div id="carousel--materials" class="owl-carousel">
            <?php foreach ($slider2 as $item): ?>
                <div>
                    <div class="img--material"><img class="img-single--material" src="<?php echo $item['image']; ?>"></div>
                    <div class="p--materials"><?php echo $item['title']; ?></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-2 button--carousel">
        <i id="right--materials" class="glyphicon glyphicon-triangle-right"></i>
    </div>
</section>

<section class="differencies">
    <div class="col-md-12">
        <h3>Чем мы отличаемся?</h3>
    </div>
    <div class="col-md-12 cont--differencies">
        <?php foreach ($differencies as $item): ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="img--differencies">
                    <img src="<?php echo $item['image']; ?>">
                </div>
                <div class="p--differencies"><?php echo $item['title']; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="benefits">
    <div class="badge--benefits">
    </div>
    <div class="col-md-12">
        <div class="col-md-8">
            <div class="badge-2--benefits">
                <img src="/public/img/badge-2.png">
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-sm-offset-3 roof">
            <div class="roof--benefits">
                <img src="<?php echo $new_block[0]['image']; ?>">
            </div>
            <div class="text--roof"><?php echo $new_block[0]['text']; ?>
            </div>
        </div>
        <div class="col-md-8 cont--benefits">
            <?php for ($i = 0; $i < count($new); $i++): ?>
                <?php echo $i == 0 ? '<div class="col-md-6 col-1--benefits">' : ''; ?>

                <?php if ($i == 5): ?>
                    </div>
                    <div class="col-md-6 col-1--benefits">
                <?php endif; ?>

                <div class="cont--benefit">
                    <div class="icon--benefit">
                        <img src="<?php echo $new[$i]['image']; ?>">
                    </div>
                    <div class="text--benefit">
                        <h6><?php echo $new[$i]['title']; ?></h6>
                        <div class="p--benefit"><?php echo $new[$i]['text']; ?>
                        </div>
                    </div>
                </div>

                <?php echo $i == 11 ? '</div>' : ''; ?>
            <?php endfor; ?>
        </div>
    </div>
</section>

<section class="download">
    <div class="col-md-12">
        <h3><?php echo $price_list[0]['title']; ?></h3>
        <a href="#"><button><?php echo $price_list[0]['text']; ?></button></a>
    </div>
</section>

<?php include_once ROOT . '/site/View/footer.php'; ?>