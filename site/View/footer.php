<section class="map">
    <a class="dg-widget-link" href="http://2gis.kz/astana/firm/70000001018129071/center/71.453769,51.190837/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Астаны</a><div class="dg-widget-link"><a href="http://2gis.kz/astana/center/71.453769,51.190837/zoom/16/routeTab/rsType/bus/to/71.453769,51.190837╎Мир кровли Астана, торговая компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Мир кровли Астана, торговая компания</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":600,"borderColor":"#a3a3a3","pos":{"lat":51.190837,"lon":71.453769,"zoom":16},"opt":{"city":"astana"},"org":[{"id":"70000001018129071"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
</section>

<footer id="block_footer">
    <div class="col-md-12">
        <div class="copyright">
            	<span>Все права защищены © 2016 | Разработано <a href="http://scode.kz">ИСХОДНЫМ КОДОМ</a>
        </div>
        <div class="bottom--footer">
            <div class="col-xs-12 logo--footer">
                <img src="<?php echo $information[0]['image_footer']; ?>" alt="Мир кровли">
            </div>
            <div class="col-xs-12 info--footer">
                <h3>Контакты</h3>
                <div class="info-single"><i class="glyphicon glyphicon-home"></i><span><?php echo $information[0]['address']; ?></span>
                </div>
                <div class="info-single"><i class="glyphicon glyphicon-earphone"></i><span><?php echo $information[0]['phone']; ?></span>
                </div>
                <div class="info-single"><i class="glyphicon glyphicon-envelope"></i><span><?php echo $information[0]['email']; ?></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
<script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?wcb_code=3b87d922b2072c015d9a6366c8e217a0" charset="UTF-8" async></script>
</body>
</html>
