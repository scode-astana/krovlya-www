<?php
session_start();

require_once 'components/Autoload.php';
require_once 'components/Router.php';
define('ROOT', dirname(__FILE__));
date_default_timezone_set('Asia/Almaty');
$url = explode('/', $_SERVER['REQUEST_URI']);
$routes = include(ROOT . '/components/Routes.php');
$user_per = \admin\Models\Users\Users::findBy('id', $_SESSION['id'])[0]['user_group'];
if (100 == $user_per) {
    Router::addRoute($routes);
    Router::dispatch(null, 'admin');
} else {
    $index = new \admin\Controllers\Index();
    $index->Index();
}