<?php

namespace core;

use components\Db;

abstract class Model
{

    const TABLE = '';

    public $id;

    public static function findAll($order_by = 'id', $sorting = 'ASC')
    {
        $db = new Db();
        return $db->query(
            "SELECT * FROM " . static::TABLE . " ORDER BY $order_by $sorting"
        );
    }

    public static function findBy($param, $value, $order_by = 'id', $sorting = 'ASC')
    {
        $db = new Db();
        return $db->query(
            "SELECT * FROM " . static::TABLE . " WHERE $param='$value' ORDER BY $order_by $sorting"
        );
    }

    public static function findInTable($table, $param = null, $value = null, $order_by = 'id', $sorting = 'ASC')
    {
        $db = new Db();
        if ($param != null) {
            return $db->query(
                "SELECT * FROM $table WHERE $param='$value' ORDER BY $order_by $sorting"
            );
        } else {
            return $db->query(
                "SELECT * FROM $table"
            );
        }
    }

    public static function sql($sql)
    {
        $db = new Db();
        return $db->query($sql);
    }

    public static function getLast()
    {
        $db = new Db();
        return $db->query(
            "SELECT * FROM " . static::TABLE . " ORDER BY id DESC LIMIT 1"
        );
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function insert()
    {
        if (!$this->isNew()) {
            return;
        }

        $columns = array();
        $values = array();
        foreach ($this as $k => $v) {
            if ('id' == $k) {
                continue;
            }
            $columns[] = $k;
            $values[$k] = "'" . $v . "'";
        }

        $sql = 'INSERT INTO ' . static::TABLE . ' (' . implode(', ', $columns) . ') VALUES (' . implode(', ', $values) . ')';
        $db = new Db();
        if ($db->query($sql)){
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $param = null, $value = null){

        if ($param != null) {
            $values = ["$param='$value'"];
        } else {
            $values = array();
            foreach ($this as $k => $v) {
                if ('id' == $k) {
                    continue;
                }
                $values[] = $k . '=' . "'" . $v . "'";
            }
        }

        $sql = 'UPDATE ' . static::TABLE . ' SET ' . implode(', ', $values) . ' WHERE id=' . $id;
        $db = new Db();
        if ($db->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBy($param = 'id', $value)
    {
        $sql = 'DELETE FROM ' . static::TABLE . ' WHERE ' . $param . '=' . $value;
        $db = new Db();
        $db->query($sql);
    }
}