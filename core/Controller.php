<?php

namespace core;

use admin\Models\Information\Information;

abstract class Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = new View();
        $this->view->information = Information::findBy('id', '1');
        if ($_SESSION['id'] != null) {
            $this->view->user = \admin\Models\Users\Users::findBy('id', $_SESSION['id']);
        }
    }

}
