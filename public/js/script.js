// JavaScript Document
$(document).ready(function() {
		
 	$("#carousel--materials").owlCarousel({
	  itemsCustom : [
        [0, 1],
        [450, 2],
        [600, 3],
        [992, 4]],
	});
 	var owl = $("#carousel--materials").data('owlCarousel');
	
	$('#left--materials').click(function() {
		owl.prev()
	});
	$('#right--materials').click(function() {
		owl.next()
	});
});